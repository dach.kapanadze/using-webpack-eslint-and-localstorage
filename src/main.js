import './styles/style.css';
import { JoinUsSection, SectionCreatorFactory } from './join-us-section';

const sectionCreatorFactory = new SectionCreatorFactory();
const joinUsSectionInstance = sectionCreatorFactory.createSection('standard');
joinUsSectionInstance.render();

document.addEventListener('DOMContentLoaded', function() {
  const sectionCreator = new SectionCreatorFactory();

  const section = sectionCreator.createSection('standard');
  section.render();
});

